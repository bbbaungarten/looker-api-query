import requests
import json
from dotenv import load_dotenv
import os
import pandas as pd
from io import StringIO
import sys

load_dotenv()

class LookerAPI():
    
    def __init__(self, slug):
        
        self.token = None
        self.host = 'https://{}'.format(os.environ.get("LOOKER_HOST"))
        self.session = requests.Session()
        self.client_id = os.environ.get("CLIENT_ID")
        self.client_secret = os.environ.get("CLIENT_SECRET")
        self.slug = slug
        
        self.auth()
        
        self.get_data()
        
    def auth(self):
        
        url = '{}{}'.format(self.host, 'login')
        params = {
            'client_id': self.client_id,
            'client_secret': self.client_secret
        }
        
        r = self.session.post(url, params)
        
        token = r.json().get('access_token')
        
        head = {'Authorization': 'token {}'.format(token)}
        
        self.session.headers.update(head)
        
    def run_query(self, query_id, data_format):
        url = '{}{}/{}/run/{}'.format(self.host, 'queries', query_id, data_format)
        
        params = {
            'limit': '-1'
        }

        r = self.session.get(url, params=params)
        
        return StringIO(r.text)
        
    def query_slug(self, slug):
        url = '{}queries/{}/{}'.format(self.host, 'slug', slug)
        
        r = self.session.get(url).json()
        
        return int(r['id'])
    
    def get_data(self, data_format='csv'):
            
        data = pd.read_csv(self.run_query(self.query_slug(self.slug), data_format))
        
        data.to_csv('data.csv', index=False)
        
api = LookerAPI(sys.argv[1])