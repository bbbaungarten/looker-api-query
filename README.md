# Looker API Query Template

This repository presents a template for querying data from looker.

## Dependencies

- `pandas`
- `python-dotenv`
- A `.env` file containing your API3 Looker `CLIENT_ID`, `CLIENT_SECRET` and the address of the Looker instance named as `LOOKER_HOST` in the following format: `<instance_address>/api/3.1`.

## How to use

To fetch data from Looker API you will need a **query slug**. 

After getting the **query slug** simply run: `python script.py <query_slug>` without the `"< >"`.

This will generated a `.csv` file containing your data.
